﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace Plagiarism.Comparer.Test
{
    [TestClass]
    public class ComparerTests
    {
        [TestMethod]
        public void Should_Return_Empty_Histogram_When_Source_Line_Is_Null_Or_Empty()
        {
            var sourceHistogram = new HistogramList(null);

            Assert.IsNotNull(sourceHistogram);
            Assert.IsNotNull(sourceHistogram.Info);
            Assert.AreEqual(0, sourceHistogram.Info.Count);

            sourceHistogram = new HistogramList(new List<string> { });

            Assert.IsNotNull(sourceHistogram);
            Assert.IsNotNull(sourceHistogram.Info);
            Assert.AreEqual(0, sourceHistogram.Info.Count);

            sourceHistogram = new HistogramList(new List<string> { "", " ", "  " });

            Assert.IsNotNull(sourceHistogram);
            Assert.IsNotNull(sourceHistogram.Info);
            Assert.AreEqual(2, sourceHistogram.Info.Count);
        }

        [TestMethod]
        public void Can_Compute_Histogram_For_Text()
        {
            //97 -a , 98 -b , 99 - c , 32 ' '
            var sourceHistogram = new HistogramList(new List<string> { "aa", "bb", "  abc" });

            Assert.AreEqual("aa", sourceHistogram.Info[0].Line);
            Assert.AreEqual(2,sourceHistogram.Info[0].Histogram[97]);

            Assert.AreEqual("bb", sourceHistogram.Info[1].Line);
            Assert.AreEqual(2, sourceHistogram.Info[1].Histogram[98]);

            Assert.AreEqual("  abc", sourceHistogram.Info[2].Line);
            Assert.AreEqual(2, sourceHistogram.Info[2].Histogram[32]);
            Assert.AreEqual(1, sourceHistogram.Info[2].Histogram[97]);
            Assert.AreEqual(1, sourceHistogram.Info[2].Histogram[98]);
            Assert.AreEqual(1, sourceHistogram.Info[2].Histogram[99]);
        }

        [TestMethod]
        public void When_Source_Document_Is_Empty_Should_Return_Empty_Correlations()
        {
            var exsitingDocument = new List<string>() { "test1", "test2" };
            var sourceDocument = new List<string>() { " ", " " };

            var sourceHistogram = new HistogramList(sourceDocument);
            var existingHistogram = new HistogramList(exsitingDocument);
            var correlationLines = sourceHistogram.Compare(existingHistogram);

            Assert.AreEqual(0, correlationLines.Count);

            sourceDocument = new List<string>() { "" };

            sourceHistogram = new HistogramList(sourceDocument);
            correlationLines = sourceHistogram.Compare(existingHistogram);

            Assert.AreEqual(0, correlationLines.Count);
        }

        [TestMethod]
        public void Can_Compare_Lines()
        {
            var exsitingDocument = new List<string>() { "test1", "test2 234 2 234234ds" };
            var sourceDocument = new List<string>() { "test1", "test2 adf dsfd sdf" };

            var sourceHistogram = new HistogramList(sourceDocument,80);
            var existingHistogram = new HistogramList(exsitingDocument);
            var correlationLines = sourceHistogram.Compare(existingHistogram);

            Assert.AreEqual("test1", correlationLines[0].ComparedLine);
            Assert.AreEqual("test1", correlationLines[0].SourceLine);
            Assert.AreEqual(100, correlationLines[0].CoverageRatio);
        }
    }
}

