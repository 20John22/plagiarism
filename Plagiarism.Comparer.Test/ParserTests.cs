﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Plagiarism.Comparer.Test
{
    [TestClass]
    public class ParserTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            var exsitingDocument = new List<string>() { "test1", "test2 234 2 234234ds" };
            var sourceDocument = new List<string>() { "test1", "test2 adf dsfd sdf" };

            var sourceHistogram = new HistogramList(sourceDocument, 40);
            var existingHistogram = new HistogramList(exsitingDocument);
            var correlationLines = sourceHistogram.Compare(existingHistogram);

            using (var str = new CorrelationStream(@"C:\Users\Jan\Repository\PersonalKanban\testxx.txt")) 
            {
                str.Start("test");
                str.Serialize(correlationLines, "testxFile");
                str.Serialize(correlationLines, "testxFile222");
                str.Stop();
            }

        }
    }
}
