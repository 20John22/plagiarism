﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Plagiarism.Comparer
{
    public class Correlation
    {
        public double CoverageRatio { get; set; }
        public string SourceLine { get; set; }
        public string ComparedLine { get; set; }
    }
}
