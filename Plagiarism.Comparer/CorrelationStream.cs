﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Plagiarism.Comparer
{
    public class CorrelationStream : IDisposable
    {
        private StreamWriter stream;

        public CorrelationStream(string path) 
        {
            try
            {
                stream = new StreamWriter(path);
            }
            catch(Exception ex)
            {
                throw new Exception("Problem opening plagiarism result file.", ex);
            }
        }

        public void Start(string fileName)
        {
            try
            {
                stream.Write(string.Format("<root><title_orignal>{0}</title_orignal>", fileName));
            }
            catch (Exception ex)
            {
                throw new Exception("Problem writing plagiarism result to output file.", ex);
            }
        }

        public void StartSerialize(string comparedFileName)
        {
            try
            {
                stream.Write(string.Format("<file><title>{0}</title>", comparedFileName));
            }
            catch (Exception ex)
            {
                throw new Exception("Problem writing plagiarism result to output file.", ex);
            }
        }

        public void EndSerialize()
        {
            try
            {
                stream.Write("</file>");
            }
            catch (Exception ex)
            {
                throw new Exception("Problem writing plagiarism result to output file.", ex);
            }
        }


        public void Serialize(Correlation correlation)
        {
            try
            {
                stream.Write(string.Format("<plagiarism><orginal_phrase>{0}</orginal_phrase><pattern_phrase>{1}</pattern_phrase><coverage_ratio>{2}</coverage_ratio></plagiarism>",
                    correlation.SourceLine, correlation.ComparedLine, correlation.CoverageRatio));         
            }
            catch (Exception ex)
            {
                throw new Exception("Problem writing plagiarism result to output file.", ex);
            }
        }

        public void Serialize(List<Correlation> correlations, string comparedFileName)
        {
            try
            {
                stream.Write(string.Format("<file><title>{0}</title>", comparedFileName));
                foreach (var correlation in correlations)
                {
                    stream.Write(string.Format("<plagiarism><orginal_phrase>{0}</orginal_phrase><pattern_phrase>{1}</pattern_phrase><coverage_ratio>{2}</coverage_ratio></plagiarism>",
                        correlation.SourceLine, correlation.ComparedLine, correlation.CoverageRatio));
                }
                stream.Write("</file>");
            }
            catch (Exception ex)
            {
                throw new Exception("Problem writing plagiarism result to output file.", ex);
            }
        }

        public void Stop()
        {
            try
            {
                stream.Write("</root>");
            }
            catch (Exception ex)
            {
                throw new Exception("Problem writing plagiarism result to output file.", ex);
            }
        }

        private bool disposed = false;
        public void Dispose()
        {
            Dispose(true);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (stream != null)
                        stream.Close();
                }

                disposed = true;
            }
        }
    }
}

//<root>
//        <title_orignal></title_orignal>
//        <file>
//                <title></title>
//                <plagiarism>
//                        <orginal_phrase></orginal_phrase>
//                        <pattern_phrase></pattern_phrase>
//                        <coverage_ratio></coverage_ratio>
//                </plagiarism>
//                <plagiarism>
//                        <orginal_phrase></orginal_phrase>
//                        <pattern_phrase></pattern_phrase>
//                        <coverage_ratio></coverage_ratio>
//                </plagiarism>
//                <plagiarism>
//                        <orginal_phrase></orginal_phrase>
//                        <pattern_phrase></pattern_phrase>
//                        <coverage_ratio></coverage_ratio>
//                </plagiarism>
//        </file>
//        <file>
//                <title></title>
//                <plagiarism>
//                        <orginal_phrase></orginal_phrase>
//                        <pattern_phrase></pattern_phrase>
//                        <coverage_ratio></coverage_ratio>
//                </plagiarism>
//                <plagiarism>
//                        <orginal_phrase></orginal_phrase>
//                        <pattern_phrase></pattern_phrase>
//                        <coverage_ratio></coverage_ratio>
//                </plagiarism>
//                <plagiarism>
//                        <orginal_phrase></orginal_phrase>
//                        <pattern_phrase></pattern_phrase>
//                        <coverage_ratio></coverage_ratio>
//                </plagiarism>
//        </file>
//</root>