﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Plagiarism.Comparer
{
    public class HistogramInfo 
    {
        public static readonly int HistogramLenght = 256;
        public HistogramInfo(string line)
        {
            _line = line;
            _histogram = new int[HistogramLenght];
            for (int i = 0; i < HistogramLenght; i++)
            {
                _histogram[i] = 0;
            }
            ComputeHistogram();
        }

        private void ComputeHistogram()
        {
            foreach (var character in _line)
            {
                if ((int)character < HistogramInfo.HistogramLenght)
                    _histogram[(int)character]++;
            }

            AvgHistogram = _histogram.Average();
            SumHistogram = _histogram.Sum();
            AcceptedPercentOfSum = (double)SumHistogram * MistakeAcceptance;
        }

        public static double MistakeAcceptance = 0.25;
        public double AcceptedPercentOfSum { get; set; }
        public int SumHistogram { get; set; }
        public double AvgHistogram { get; set; }


        private int[] _histogram;
        private string _line;

        public int[] Histogram { get { return _histogram; } }
        public string Line { get { return _line; } }
    }
}
