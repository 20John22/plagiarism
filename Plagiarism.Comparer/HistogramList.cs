﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Plagiarism.Comparer
{
    public class HistogramList
    {
        private List<HistogramInfo> _info;
        public List<HistogramInfo> Info 
        {
            get 
            {
                if (_info == null)
                    _info = new List<HistogramInfo>();
                return _info;
            }
        }
        private CorrelationStream _stream;

        private double _acceptedCorrelation;

        public HistogramList(List<string> sourceLines)
            : this(sourceLines, 70, null)
        {
        }
        public HistogramList(List<string> sourceLine, int acceptedCorrelation)
            : this(sourceLine, acceptedCorrelation, null)
        {
        }
        public HistogramList(List<string> sourceLines, CorrelationStream stream)
            : this(sourceLines, 70, stream)
        {
        }
        public HistogramList(List<string> sourceLines, int acceptedCorrelation, CorrelationStream stream)
        {
            _stream = stream;
            _acceptedCorrelation = acceptedCorrelation;
            if (sourceLines == null)
                return;

            sourceLines.RemoveAll(x => string.IsNullOrEmpty(x));
            if (sourceLines.Count == 0)
                return;

            this._info = sourceLines.Select( x=> new HistogramInfo(x)).ToList();
        }


        public List<Correlation> Compare(HistogramList comparedHistogram)
        {
            var correlations = new List<Correlation>();
            foreach (var sourceLine in _info)
            {
                foreach (var comparedLine in comparedHistogram.Info)
                {
                    var coverage = ComputeCoverageNormalized(sourceLine, comparedLine);

                    if (coverage > 0)
                        _stream.Serialize(new Correlation { SourceLine = sourceLine.Line, ComparedLine = comparedLine.Line, CoverageRatio = coverage });
                        //correlations.Add(new Correlation { So__urceLine = sourceLine.Line, ComparedLine = comparedLine.Line, CoverageRatio = coverage });
                }
            }

            return correlations;
        }

        private static double ComputeCoverageNormalized(HistogramInfo sourceHistogram, HistogramInfo comparedHistogram)
        {
            var temp = new int[HistogramInfo.HistogramLenght];
            for (int i = 0; i < HistogramInfo.HistogramLenght; i++)
            {
                temp[i] = Math.Abs( sourceHistogram.Histogram[i] - comparedHistogram.Histogram[i]);
            }
            var sum = temp.Sum();


            if (sourceHistogram.AcceptedPercentOfSum > sum)
                return 100*(sourceHistogram.SumHistogram-sum)/sourceHistogram.SumHistogram;
            return 0;

        }

    }
}
