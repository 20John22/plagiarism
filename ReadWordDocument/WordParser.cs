﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Application = System.Windows.Forms.Application;
using System.Collections.Generic;
using Word;

namespace Plagiarism
{
    public static class WordParser
    {
        public static List<string> ReadFileContent(string path, decimal dlugoscPomijanychAkapitow, decimal dlugoscPomijanychLinii)
        {
            List<string> result = new List<string>();
            ApplicationClass wordApp = new ApplicationClass();
            object file = path;
            object nullobj = System.Reflection.Missing.Value;
            Document doc = new Document();
            IDataObject data;
            for (; ; )
            {
                try
                {
                    doc = wordApp.Documents.Open(
               ref file, ref nullobj, ref nullobj,
               ref nullobj, ref nullobj, ref nullobj,
               ref nullobj, ref nullobj, ref nullobj,
               ref nullobj, ref nullobj, ref nullobj);
                    doc.ActiveWindow.Selection.WholeStory();
                    doc.ActiveWindow.Selection.Copy();
                    data = Clipboard.GetDataObject();
                    break;
                }
                catch (Exception ex)
                {
                    var aaa = ex.Message;
                }


            }


            var paragraphs = data.GetData(DataFormats.Text).ToString().Split('\n');

            foreach (var paragraph in paragraphs)
            {
                //  var line = paragraph.Split(' ');
                if (paragraph.Split(' ').Length < dlugoscPomijanychAkapitow)
                {
                    continue;
                }
                var redyToGo = paragraph.Split('.');
                foreach (var item in redyToGo)
                {
                    var line = item.TrimStart().TrimEnd();
                    if (line == "")
                    {
                        continue;
                    }
                    // sprawdzenie czy linia zaczyna się od wielkiej litery
                  if ((int)line[0] > 90)
                    {
                        string previous = result[result.Count - 1];
                        result.RemoveAt(result.Count - 1);
                        previous = previous + " " + line;
                        if (line.Contains("<"))
                        {
                            line = line.Replace('<', 'x');
                        }
                        if (line.Contains(">"))
                        {
                            line = line.Replace('>', 'x');
                        }
						if (line.Contains("&&"))
						{
							line = line.Replace("&&", " ");
						}
                        //if (line.Contains("/>"))
                        //{
                        //    line.Replace("/>", ' ');
                        //}
                        result.Add(previous);
                    }
                    else
                    {

                        if (line.Length < dlugoscPomijanychLinii)
                        {
                            continue;
                        }
                        if (line.Contains("<"))
                        {
                            line = line.Replace('<', 'x');
                        }
                        if (line.Contains(">"))
                        {
                            line = line.Replace('>', 'x');
                        }
						if (line.Contains("&&"))
						{
							line = line.Replace("&&", " ");
						}
                        result.Add(line);

                    }
                }

            }
            doc.Close(ref nullobj, ref nullobj, ref nullobj);
            wordApp.Quit(ref nullobj, ref nullobj, ref nullobj);
            return result;
        }
    }
}
