using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Office;
using Word;
using Application = System.Windows.Forms.Application;

namespace ReadWordDocument
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : Form
	{
		private Label label1;
		private TextBox txtPathName;
		private Button btnRead;
		private TextBox txtFileContent;
		private Button btnBrowse;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.txtPathName = new System.Windows.Forms.TextBox();
			this.btnRead = new System.Windows.Forms.Button();
			this.txtFileContent = new System.Windows.Forms.TextBox();
			this.btnBrowse = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 48);
			this.label1.Name = "label1";
			this.label1.TabIndex = 0;
			this.label1.Text = "Word File Name";
			// 
			// txtPathName
			// 
			this.txtPathName.Location = new System.Drawing.Point(112, 48);
			this.txtPathName.Name = "txtPathName";
			this.txtPathName.Size = new System.Drawing.Size(344, 20);
			this.txtPathName.TabIndex = 1;
			this.txtPathName.Text = "";
			// 
			// btnRead
			// 
			this.btnRead.Location = new System.Drawing.Point(112, 80);
			this.btnRead.Name = "btnRead";
			this.btnRead.TabIndex = 2;
			this.btnRead.Text = "Read";
			this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
			// 
			// txtFileContent
			// 
			this.txtFileContent.Location = new System.Drawing.Point(16, 152);
			this.txtFileContent.Multiline = true;
			this.txtFileContent.Name = "txtFileContent";
			this.txtFileContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.txtFileContent.Size = new System.Drawing.Size(520, 160);
			this.txtFileContent.TabIndex = 3;
			this.txtFileContent.Text = "";
			// 
			// btnBrowse
			// 
			this.btnBrowse.Location = new System.Drawing.Point(464, 48);
			this.btnBrowse.Name = "btnBrowse";
			this.btnBrowse.TabIndex = 4;
			this.btnBrowse.Text = "Browse";
			this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(544, 334);
			this.Controls.Add(this.btnBrowse);
			this.Controls.Add(this.txtFileContent);
			this.Controls.Add(this.btnRead);
			this.Controls.Add(this.txtPathName);
			this.Controls.Add(this.label1);
			this.Name = "Form1";
			this.Text = "Read a Word Document";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void btnRead_Click(object sender, EventArgs e)
		{
			if(txtPathName.Text.Length>0)
			{
				readFileContent(txtPathName.Text);
			}
			else
			{
				MessageBox.Show("Enter a valid file path");
			}
		}

		private void btnBrowse_Click(object sender, EventArgs e)
		{
			OpenFileDialog ofd=new OpenFileDialog();
			ofd.Filter="Word Documents(*.doc)|*.doc";
			ofd.ShowDialog();
			txtPathName.Text=ofd.FileName;
		}
		//To read content of a Word document using Microsoft Word Library Component
		private void readFileContent(string path)
		{
			Word.ApplicationClass wordApp=new ApplicationClass();
			object file=path;
			object nullobj=System.Reflection.Missing.Value;			
				
			Word.Document doc = wordApp.Documents.Open(
				ref file, ref nullobj, ref nullobj, 
				ref nullobj, ref nullobj, ref nullobj, 
				ref nullobj, ref nullobj, ref nullobj, 
				ref nullobj, ref nullobj, ref nullobj);
			doc.ActiveWindow.Selection.WholeStory();
			doc.ActiveWindow.Selection.Copy();
			IDataObject data=Clipboard.GetDataObject();
			txtFileContent.Text=data.GetData(DataFormats.Text).ToString();
			doc.Close(ref nullobj,ref nullobj,ref nullobj);
			wordApp.Quit(ref nullobj,ref nullobj,ref nullobj);
			
		}
	}
}
