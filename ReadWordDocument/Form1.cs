using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Application = System.Windows.Forms.Application;
using System.Collections.Generic;
using Word;
using Plagiarism.Comparer;
using System.Linq;
using System.Diagnostics;

namespace Plagiarism.WinUi
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class Form1 : Form
    {
        private Label label1;
        private TextBox txtPathName;
        private Button btnRead;
        private TextBox txtFileContent;
        private Button btnBrowse;
        private NumericUpDown dlugoscAkapitu;
        private Label długoscAkapitu;
        private Label dlugosLiniLabel;
        private NumericUpDown dlugoscLini;
        private Label label2;
        private Button button1;

        string _folderName = string.Empty;
        private Label selectedFolder;
        private Label label3;


        // zawartość pliku plagiatów i kolejnych plików wzorcowych
        List<string> plagiatFile = new List<string>();
        List<string> oryginalFile = new List<string>();
        private Label label4;
        private NumericUpDown procentZgodnosci;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private Container components = null;

        public Form1()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtPathName = new System.Windows.Forms.TextBox();
            this.btnRead = new System.Windows.Forms.Button();
            this.txtFileContent = new System.Windows.Forms.TextBox();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.dlugoscAkapitu = new System.Windows.Forms.NumericUpDown();
            this.długoscAkapitu = new System.Windows.Forms.Label();
            this.dlugosLiniLabel = new System.Windows.Forms.Label();
            this.dlugoscLini = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.selectedFolder = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.procentZgodnosci = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.dlugoscAkapitu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dlugoscLini)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.procentZgodnosci)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Plik do zbadania";
            // 
            // txtPathName
            // 
            this.txtPathName.Location = new System.Drawing.Point(112, 48);
            this.txtPathName.Name = "txtPathName";
            this.txtPathName.Size = new System.Drawing.Size(344, 20);
            this.txtPathName.TabIndex = 1;
            // 
            // btnRead
            // 
            this.btnRead.Location = new System.Drawing.Point(112, 80);
            this.btnRead.Name = "btnRead";
            this.btnRead.Size = new System.Drawing.Size(120, 44);
            this.btnRead.TabIndex = 2;
            this.btnRead.Text = "Analizuj!";
            this.btnRead.Click += new System.EventHandler(this.btnRead_Click);
            // 
            // txtFileContent
            // 
            this.txtFileContent.Location = new System.Drawing.Point(19, 385);
            this.txtFileContent.Multiline = true;
            this.txtFileContent.Name = "txtFileContent";
            this.txtFileContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtFileContent.Size = new System.Drawing.Size(520, 160);
            this.txtFileContent.TabIndex = 3;
            this.txtFileContent.WordWrap = false;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(464, 48);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnBrowse.TabIndex = 4;
            this.btnBrowse.Text = "Wskaż plik";
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // dlugoscAkapitu
            // 
            this.dlugoscAkapitu.Location = new System.Drawing.Point(19, 250);
            this.dlugoscAkapitu.Name = "dlugoscAkapitu";
            this.dlugoscAkapitu.Size = new System.Drawing.Size(52, 20);
            this.dlugoscAkapitu.TabIndex = 5;
            // 
            // długoscAkapitu
            // 
            this.długoscAkapitu.AutoSize = true;
            this.długoscAkapitu.Location = new System.Drawing.Point(86, 252);
            this.długoscAkapitu.Name = "długoscAkapitu";
            this.długoscAkapitu.Size = new System.Drawing.Size(320, 13);
            this.długoscAkapitu.TabIndex = 6;
            this.długoscAkapitu.Text = "Długość akapitów, które mają zostać pominięte (testowane przy 4)";
            // 
            // dlugosLiniLabel
            // 
            this.dlugosLiniLabel.AutoSize = true;
            this.dlugosLiniLabel.Location = new System.Drawing.Point(86, 288);
            this.dlugosLiniLabel.Name = "dlugosLiniLabel";
            this.dlugosLiniLabel.Size = new System.Drawing.Size(297, 13);
            this.dlugosLiniLabel.TabIndex = 8;
            this.dlugosLiniLabel.Text = "Długość linii, które mają zostać pominięte (testowane przy 15)";
            // 
            // dlugoscLini
            // 
            this.dlugoscLini.Location = new System.Drawing.Point(19, 286);
            this.dlugoscLini.Name = "dlugoscLini";
            this.dlugoscLini.Size = new System.Drawing.Size(52, 20);
            this.dlugoscLini.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 360);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(133, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Zawartość badanego pliku";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(342, 118);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(197, 23);
            this.button1.TabIndex = 10;
            this.button1.Text = "Wskaż folder z pracami wzorcowymi";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // selectedFolder
            // 
            this.selectedFolder.AutoSize = true;
            this.selectedFolder.Location = new System.Drawing.Point(16, 181);
            this.selectedFolder.Name = "selectedFolder";
            this.selectedFolder.Size = new System.Drawing.Size(0, 13);
            this.selectedFolder.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 153);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Folder z pracami:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(86, 217);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(158, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Procent zgodności z oryginałem";
            // 
            // procentZgodnosci
            // 
            this.procentZgodnosci.Location = new System.Drawing.Point(19, 215);
            this.procentZgodnosci.Name = "procentZgodnosci";
            this.procentZgodnosci.Size = new System.Drawing.Size(52, 20);
            this.procentZgodnosci.TabIndex = 13;
            this.procentZgodnosci.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(571, 570);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.procentZgodnosci);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.selectedFolder);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dlugosLiniLabel);
            this.Controls.Add(this.dlugoscLini);
            this.Controls.Add(this.długoscAkapitu);
            this.Controls.Add(this.dlugoscAkapitu);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.txtFileContent);
            this.Controls.Add(this.btnRead);
            this.Controls.Add(this.txtPathName);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Read a Word Document";
            ((System.ComponentModel.ISupportInitialize)(this.dlugoscAkapitu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dlugoscLini)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.procentZgodnosci)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.Run(new Form1());
        }

        private string GetFileName(string path)
        {
            if (string.IsNullOrEmpty(path))
                return string.Empty;

            return path.Split('\\').Last();
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            if (txtPathName.Text.Length > 0 && _folderName.Length > 0)
            {
                plagiatFile = WordParser.ReadFileContent(txtPathName.Text, dlugoscAkapitu.Value, dlugoscLini.Value);
                HistogramInfo.MistakeAcceptance = Convert.ToDouble((100 - procentZgodnosci.Value) / 100);
                // powyzej akceptowalny blad, wartosc 0.25 oznacz iz akceptujemy linie z bledem 25%
               

                // wyswietlenie całego tekstu
                //         txtFileContent.Text = data.GetData(DataFormats.Text).ToString();
                //txtFileContent.Text = "";
                //foreach (var line in plagiatFile)
                //{
                //    txtFileContent.Text += line + Environment.NewLine;
                //}
                 
                string[] filePaths = Directory.GetFiles(@_folderName + "\\", "*.DOC");

                //sciezka do pliku wyjsciowego z xml
                var resultPath = "result.xml";
                using (var stream = new CorrelationStream(resultPath))
                {
                    var sourceHistogram = new HistogramList(plagiatFile, stream);

                    stream.Start(GetFileName(txtPathName.Text));
                    foreach (var file in filePaths)
                    {
                        stream.StartSerialize(GetFileName(file));
                        oryginalFile = WordParser.ReadFileContent(file, dlugoscAkapitu.Value, dlugoscLini.Value);
                        sourceHistogram.Compare(new HistogramList(oryginalFile,70,stream));
                    
                        stream.EndSerialize();
                    }
                    stream.Stop();

                    //display results
                    string path = Directory.GetCurrentDirectory() + "\\index.html";
                    path = path.Replace("\\", "/");
                    string link = @"file:///" + path;
                    Process.Start(link);
                }

                MessageBox.Show("Analiza zakończona.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Musisz najpierw wskazać folder z pracami wzorcowyki, oraz plik do testu.", "Informacja", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Word Documents(*.docx;*.doc)|*.docx;*.doc";
            ofd.ShowDialog();
            txtPathName.Text = ofd.FileName;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _folderName = (System.IO.Directory.Exists(_folderName)) ? _folderName : "";
            var dlg1 = new Ionic.Utils.FolderBrowserDialogEx
            {
                Description = "Wybierz folder:",
                ShowNewFolderButton = true,
                ShowEditBox = true,
                //NewStyle = false,
                SelectedPath = _folderName,
                ShowFullPathInEditBox = false,
            };
            dlg1.RootFolder = System.Environment.SpecialFolder.MyComputer;

            var result = dlg1.ShowDialog();

            if (result == DialogResult.OK)
            {
                _folderName = dlg1.SelectedPath;
                this.selectedFolder.Text = _folderName;
            }
        }
    }
}
